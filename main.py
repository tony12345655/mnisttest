#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time : 2021/12/30 下午 04:28
# @Author : Aries
# @Site : 
# @File : mlp.py
# @Software: PyCharm


from keras.datasets import mnist
from keras.models import Sequential
from keras.layers import Dense, Activation, Dropout, Dense, Dropout, Flatten, Conv2D, MaxPooling2D, Input, SimpleRNN, \
    RNN
from keras.utils import np_utils
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt


def mlp():
    # 載入Mnist資料集
    (imgTrain, labelTrain), (imgTest, labelTest) = mnist.load_data()
    # 將訓練資料與測試資料攤平
    imgTrain = imgTrain.reshape(imgTrain.shape[0], 28 * 28).astype('float32')
    imgTest = imgTest.reshape(imgTest.shape[0], 28 * 28).astype('float32')
    # 影像標準化
    imgTrain /= 255
    imgTest /= 255
    # 類別的數目
    nb_classes = 10
    # 轉為One Hot 格式
    labelTrain = np_utils.to_categorical(labelTrain, nb_classes)
    labelTest = np_utils.to_categorical(labelTest, nb_classes)
    # 宣告優化器
    model = Sequential()
    # 設置隱藏層、激勵函數
    model.add(Dense(50, input_shape=(784,)))
    model.add(Dense(units=nb_classes))
    model.add(Activation('softmax'))
    # 使用compile定義損失函數、優化函數、成效衡量指標
    model.compile(loss='categorical_crossentropy', optimizer='SGD', metrics=['accuracy'])
    # 模型總結
    model.summary()
    # 開始訓練
    history = model.fit(imgTrain, labelTrain, epochs=30, batch_size=128, verbose=1)
    # 畫出loss誤差值的執行結果
    plt.figure(figsize=(5, 3))
    plt.plot(history.epoch, history.history['loss'])
    plt.title('loss')
    plt.show()
    # 畫出accuracy
    plt.figure(figsize=(5, 3))
    plt.plot(history.epoch, history.history['accuracy'])
    plt.title('accuracy')
    plt.show()
    # 評估模型準確率
    scores = model.evaluate(imgTest, labelTest, verbose=2)
    print("accuracy:", scores[1] * 100, "%")


def cnn():
    (imgTrain, labelTrain), (imgTest, labelTest) = mnist.load_data()
    # 將影像特徵以reshape轉為60000*28*28*1的四維矩陣
    Train4D = imgTrain.reshape(imgTrain.shape[0], 28, 28, 1).astype('float32')
    Test4D = imgTest.reshape(imgTest.shape[0], 28, 28, 1).astype('float32')

    # 將影像特徵標準化
    Train4D_normalize = Train4D / 255  # 除以255（因為圖像的像素點介於0~255之間）
    Test4D_normalize = Test4D / 255

    # 將訓練和測試的label,進行One hot encoding轉換
    TrainOneHot = np_utils.to_categorical(labelTrain)
    TestOneHot = np_utils.to_categorical(labelTest)

    # 建立一個Sequential模型，後續add即可加入模型
    model = Sequential()
    # 建立卷積層1
    model.add(Conv2D(filters=16,  # 建立16個濾鏡
                     kernel_size=(4, 4),  # 每個濾鏡4*4的大小
                     padding='same',  # 讓卷積運算不會影響圖的大小
                     input_shape=(28, 28, 1),  # 輸入影像的形狀(影像長，影像寬,影像色階)
                     activation='relu'))  # 定義激活函數為RELU
    # 建立池化層1
    model.add(MaxPooling2D(pool_size=(2, 2)))
    # 建立卷積層2
    model.add(Conv2D(filters=36,  # 建立36個濾鏡
                     kernel_size=(4, 4),  # 每個濾鏡4*4的大小
                     padding='same',  # 讓卷積運算不會影響圖的大小
                     activation='relu'))  # 定義激活函數為RELU
    # 建立池化層2
    model.add(MaxPooling2D(pool_size=(2, 2)))
    # 加入Dropout，避免overfitting
    model.add(Dropout(0.25))
    # 建立平坦層
    model.add(Flatten())
    # 建立隱藏層
    model.add(Dense(1500, activation='relu'))
    # 建立輸出層
    model.add(Dense(10, activation='softmax'))
    # 打包
    model.summary()
    # 定義訓練方法
    model.compile(loss='categorical_crossentropy',
                  optimizer='adam', metrics=['accuracy'])
    # 訓練模型
    trainHistory = model.fit(x=Train4D_normalize,
                             y=TrainOneHot,
                             validation_split=0.2,
                             epochs=10, batch_size=300,
                             verbose=1)
    # 畫圖表(acc)
    plt.show(trainHistory.history['accuracy'])
    plt.plot(trainHistory.history['val_accuracy'])
    plt.title('Train History')
    plt.ylabel('train')
    plt.xlabel('Epoch')
    plt.legend(['train', 'validation'], loc='center right')
    plt.show()
    # 畫圖表(loss)
    plt.show(trainHistory.history['loss'])
    plt.plot(trainHistory.history['val_loss'])
    plt.title('Train History')
    plt.ylabel('train')
    plt.xlabel('Epoch')
    plt.legend(['train', 'validation'], loc='center right')
    plt.show()
    # 評估模型
    loss, accuracy = model.evaluate(Test4D_normalize, TestOneHot)
    print("\nLoss: %.2f, Accuracy: %.2f%%" % (loss, accuracy * 100))
    # 顯示混淆矩陣
    predict_x = model.predict(Test4D_normalize)
    classes_x = np.argmax(predict_x, axis=1)
    confuse_arr = pd.crosstab(labelTest, classes_x, rownames=['label'], colnames=['predict'])
    print(confuse_arr)


def rnn():
    (imgTrain, labelTrain), (imgTest, labelTest) = mnist.load_data()
    # 將影像特徵以reshape轉為60000*28*28三維矩陣
    Train3D = imgTrain.reshape(imgTrain.shape[0], 28, 28).astype('float32')
    Test3D = imgTest.reshape(imgTest.shape[0], 28, 28).astype('float32')

    # 將影像特徵標準化
    Train3D = Train3D / 255  # 除以255（因為圖像的像素點介於0~255之間）
    Test3D = Test3D / 255

    # 將訓練和測試的label,進行One hot encoding轉換
    TrainOneHot = np_utils.to_categorical(labelTrain)
    TestOneHot = np_utils.to_categorical(labelTest)
    # 建立模型
    model = Sequential()
    # 隱藏層
    model.add(SimpleRNN(
        batch_input_shape=(None, 28, 28),
        units=50,
        unroll=True,
    ))
    # 輸出層
    model.add(Dense(units=10, kernel_initializer='normal', activation='softmax'))
    # 打包
    model.summary()
    # 設定損失函數等等
    model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
    # 訓練模型
    model.fit(x=Train3D,
              y=TrainOneHot,
              validation_split=0.2,
              epochs=10, batch_size=160,
              verbose=1)
    # 評估模型
    loss, accuracy = model.evaluate(Test3D, TestOneHot)
    print("\nLoss: %.2f, Accuracy: %.2f%%" % (loss, accuracy * 100))


if __name__ == '__main__':
    # 用mlp訓練
    # mlp()
    # 用cnn訓練
    # cnn()
    # 用rnn訓練
    rnn()
